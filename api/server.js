import express from "express";
import mongoose from "mongoose";
import dotenv from "dotenv";
import userRoute from "./routes/user.route.js";
import authRoute from "./routes/auth.route.js";
import storyRoute from "./routes/story.route.js";
import commentRoute from "./routes/comment.route.js";
import cookieParser from "cookie-parser";
import cors from "cors";
import swaggerJsDoc from "swagger-jsdoc";
import swaggerUi from "swagger-ui-express";

dotenv.config();

const PORT = process.env.ENV_PORT;
const app = express();

mongoose.set('strictQuery', true);


const connect = async () =>{
    try {
        await mongoose.connect(process.env.MONGO);
        console.log("Connected to dataBase mongoDB")
    } catch (error) {
        console.log(error);
    }
};

/****************** OUTSIDE MIDDLEWARE****************/
//middleware is shared code called before each endpoints

//to allow other addresses to use the server's api
//credential true because we'll be using cookies
app.use(cors({ origin: process.env.CLIENT_URL, credentials: true }));

//middleware to parse json. Express doesn't parse req.body to JSON by default.
//using app.use(express.json) so that req (request) body coming in from user can be parsed thanks to that code
app.use(express.json());

//other parser middleware to parse cookies
app.use(cookieParser());

/****************************************************/

/*************Routes used by Express ***************/
app.use("/api/auths", authRoute);
app.use("/api/stories", storyRoute);
app.use("/api/users", userRoute);
app.use("/api/comments", commentRoute);
/****************************************************/

//for handling error messages
app.use((err,req,res,next)=>{
    const errorStatus = err.status || 500;
    const errorMsg = err.message || "Something went wrong";

    return res.status(errorStatus).send(errorMsg);
})


/****************************SWAGGER DOCS**************************/
const options = {
    definition: {
      openapi: "3.0.0",
      info: {
        title: "Rest API doc",
        version: "0.1.0",
        description:
          "Api doc for myStory app",
      },
      components: {
        securitySchemas: {
          bearerAuth: {
            type: "http",
            scheme: "bearer",
            bearerFormat: "JWT",
          },
        },
      },
      security: [
        {
          bearerAuth: [],
        },
      ],
      servers: [
        {
          url: `http://localhost:${PORT}/`,
        },
      ],
    },
    apis: ["./routes/*.js", "./schemas/*.js"],
};

const specs = swaggerJsDoc(options);
app.use(
  "/api-docs",
  swaggerUi.serve,
  swaggerUi.setup(specs)
);

app.get("/api-docs.json", (req, res) => {
  res.setHeader("Content-Type", "application/json");
  res.send(specs);
});

  /**************************************************************/


/*******SERVER START AND CONNECT TO MONGOOSE*********/
app.listen(PORT, ()=>{
    connect();
    console.log(`server is running on http://localhost:${PORT}`);
    console.log(`API DOC is running on http://localhost:${PORT}/api-docs`);
    console.log(`API DOC in JSON format for postman is running on http://localhost:${PORT}/api-docs.json`);
})
/****************************************************/

export default app;