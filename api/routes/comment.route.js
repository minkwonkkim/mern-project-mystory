import express from "express";
import { verifyToken } from "../middleware/jwt.js";
import { createComment, getComments, deleteComment, putComment } from "../controllers/comment.controller.js"

const router = express.Router();

/**
 * @openapi
 * '/api/comments':
 *  post:
 *     tags:
 *     - Comments
 *     summary: Create a comment
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/CreateCommentInput'
 *     responses:
 *        201:
 *          description: Success
 *        401: 
 *          description : have to be logged in to create a comment
 */
router.post("/", verifyToken, createComment);

/**
 * @openapi
 * '/api/comments/{storyId}':
 *  get:
 *     tags:
 *     - Comments
 *     summary: Get all the comments from a story
 *     parameters:
 *      - name: storyId
 *        in: path
 *        description: The id of the story to get the comments from
 *        required: true
 *     responses:
 *      200:
 *        description: Success
 *      404:
 *        description: Not Found
 * 
 * '/api/comments/{commentId}':
 *  put:
 *     tags:
 *     - Comments
 *     summary: Update comment from a story
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/UpdateCommentInput'
 *     parameters:
 *      - name: commentId
 *        in: path
 *        description: The id of the comment to update
 *        required: true
 *     responses:
 *      200:
 *        description: Successfully updated comment
 *      401:
 *        description: Token invalid (Not authenticated)
 *      403:
 *        description: Token invalid (comments doesn't belong to current user)
 *      404:
 *        description: Can't find comment
 *  delete:
 *     tags:
 *     - Comments
 *     summary: Delete comment from a story
 *     parameters:
 *      - name: commentId
 *        in: path
 *        description: The id of the comment to delete
 *        required: true
 *     responses:
 *      200:
 *        description: Successfully deleted comment
 *      401:
 *        description: Token invalid (Not authenticated)
 *      403:
 *        description: Token invalid (comments doesn't belong to current user)
 *      404:
 *        description: Can't find comment
 */

router.get("/:storyId", getComments);
router.put("/:commentId", verifyToken, putComment);
router.delete("/:commentId", verifyToken, deleteComment);

export default router