import express from "express";
import { deleteUser, getUser } from "../controllers/user.controller.js";
import { verifyToken } from "../middleware/jwt.js";

const router = express.Router();


/**
   * @openapi
   * '/api/users/{userId}':
   *  get:
   *     tags:
   *     - User (for admin and test purposes)
   *     summary: Get a single user by id
   *     parameters:
   *      - name: userId
   *        in: path
   *        description: The id of the user
   *        required: true
   *     responses:
   *      200:
   *        description: Success
   *  delete:
   *     tags:
   *     - User (for admin and test purposes)
   *     summary: Get a single user by id
   *     parameters:
   *      - name: userId
   *        in: path
   *        description: The id of the user
   *        required: true
   *     responses:
   *      200:
   *        description: Success
   *      404:
   *        description: Can't find user
   *      403:
   *        description: Not user
   *        
*/

router.delete("/:id", verifyToken, deleteUser);
router.get("/:id", getUser);

export default router;