import express from "express";
import { verifyToken } from "../middleware/jwt.js";
import{ createStory, deleteStory, getStory, getStories, putStory} from "../controllers/story.controller.js"

const router = express.Router();


/**
 * @openapi
 * '/api/stories':
 *  post:
 *     tags:
 *     - Stories
 *     summary: Create a story
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/CreateStoryInput'
 *     responses:
 *        201:
 *          description: Success
 *        401: 
 *          description : have to be logged in to create a story
 *  get:
 *     tags:
 *     - Stories
 *     summary: Get all stories
 *     responses:
 *      200:
 *        description: Success
 *      404:
 *        description: Not Found
 * 
 * '/api/stories/{storyId}':
 *  delete:
 *     tags:
 *     - Stories
 *     summary: Delete a story by the storyId
 *     parameters:
 *      - name: storyId
 *        in: path
 *        description: The id of the story
 *        required: true
 *     responses:
 *      200:
 *        description: Success
 *      403:
 *        description: Not the owner of story
 *      404:
 *        description: Not Found
 * 
 * '/api/stories/single/{storyId}':
 *  get:
 *     tags:
 *     - Stories
 *     summary: Get a single story by the storyId
 *     parameters:
 *      - name: storyId
 *        in: path
 *        description: The id of the story
 *        required: true
 *     responses:
 *      200:
 *        description: Success
 *      404:
 *        description: Not Found
 */

router.post("/", verifyToken, createStory);
router.get("/", getStories);
router.get("/single/:id", getStory);
router.delete("/:id", verifyToken, deleteStory);

//not useful yet
router.put("/:id", verifyToken, putStory);


export default router;