
import express from "express";
import {register, login, logout} from "../controllers/auth.controller.js"
import { verifyToken } from "../middleware/jwt.js";

const router = express.Router();

/**
 * @openapi
 * 
 * '/api/auths/register':
 *  post:
 *     tags:
 *     - Authentification
 *     summary: Register a user
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/CreateUserInput'
 *     responses:
 *        201:
 *          description: Successfully registered
 *        409:
 *          description: Already used username or email
 * 
 * '/api/auths/login':
 *  post:
 *     tags:
 *     - Authentification
 *     summary: Login a user and setup a jwt token and cookie
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/LoginUserInput'
 *     responses:
 *      200:
 *        description: Successfully logged in
 *      400:
 *        description: Wrong Credentials or not in database
 * 
 * '/api/auths/logout':
 *  post:
 *     tags:
 *     - Authentification
 *     summary: Logout a user and clear cookies
 *     responses:
 *      200:
 *        description: Successfully logged Out
 *      401:
 *        description: Already logged Out
 */

  router.post("/register", register);
  router.post("/login",login);
  router.post("/logout", verifyToken, logout);

export default router;
