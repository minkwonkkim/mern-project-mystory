import jwt from "jsonwebtoken";
import createError from "../utils/createError.js";

export const verifyToken = (req,res,next) =>{
    const token = req.cookies.accessToken;
    if(!token) return next(createError(401, "You are not authenticated"));

    jwt.verify(token, process.env.JWT_SECRET, async (err,payload)=>{
        if (err) return next(createError(403, "Token is not valid"));
        
        req.userId = payload.id;
        //next because the route to this middleware does not end the req-res cycle (no res.send)
        next();
    });
}