/**
 * @openapi
 * components:
 *   schemas:
 *     Comment:
 *       type: object
 *       required:
 *         - _id
 *         - storyId
 *         - userId
 *         - star
 *         - txt
 *       properties:
 *         _id:
 *           type: Object(string)
 *           description: The auto-generated unique id of the book given by mongoose & mongodb
 *         storyId:
 *           type: string
 *           description: The unique id of the story under which the comment is from
 *         userId:
 *           type: string
 *           description: The unique id of the user that wrote the comment
 *         star:
 *           type: number
 *           enum: [1 , 2 , 3 , 4 , 5]
 *           description: The rating of the story by the user associated with the comment
 *         txt:
 *           type: string
 *           description: The textual content of the comment
 *         createdAt:
 *           type: string
 *           format: date
 *           description: The auto-generated date of creation given by mongoose & mongodb
 * 
 *     CreateCommentInput:
 *         type: object
 *         required:
 *             - star
 *             - txt
 *             - storyId
 *         properties:
 *             txt:
 *                 type: string
 *                 default: This Story is good
 *             star:
 *                 type: number
 *                 enum: [1,2,3,4,5]
 *                 default: 3
 *             storyId:
 *                 type: string
 *                 default: existingStoryId
 * 
 *     UpdateCommentInput:
 *         type: object
 *         required:
 *             - txt
 *         properties:
 *             txt:
 *                 type: string
 *                 default: Nevermind I change my mind, this story is bad
 */

import mongoose from 'mongoose';

const { Schema } = mongoose;

const commentSchema = new Schema({
        storyId:{
                type:String,
                required: true,
        },
        userId:{
                type:String,
                required: true,
        },
        star:{
                type:Number,
                required: true,
                enum:[1,2,3,4,5],
        },
        txt:{
                type:String,
                required: true,
        },
},{
        timestamps:true
});

export default mongoose.model("Comment", commentSchema);