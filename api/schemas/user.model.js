/**
 * @openapi
 * components:
 *  schemas:
 *      User:
 *          type: object
 *          required:
 *              - _id
 *              - username
 *              - email
 *              - password
 *          properties:
 *              _id:
 *                  type: Object(string)
 *                  description: The auto-generated unique id of the book given by mongoose & mongodb
 *              username:
 *                  type: string
 *                  description: The unique username of the user
 *              email:
 *                  type: string
 *                  format: mail
 *                  description: The unique email of the user
 *              password:
 *                  type: string
 *                  description: user's password. will be encrypted when passed to database
 *              pfp:
 *                  type: string
 *                  format: link
 *                  description: user's profile picture. string is the link to the img, stored in the cloud
 *              desc:
 *                  type: string
 *                  default: ""
 *                  description: user's description provided by themselves on register
 *              createdAt:
 *                  type: string
 *                  format: date
 *                  description: The auto-generated date of creation given by mongoose & mongodb
 *      CreateUserInput:
 *          type: object
 *          required:
 *              - username
 *              - email
 *              - password
 *          properties:
 *              username:
 *                  type: string
 *                  default: Jane Doe
 *              email:
 *                  type: string
 *                  default: jane.doe@example.com
 *              password:
 *                  type: string
 *                  default: stringPassword123
 *              pfp:
 *                  type: string
 *                  default: "https://linktoimg.com"
 *              desc:
 *                  type: string
 *                  default: "My name is..."
 *      LoginUserInput:
 *          type: object
 *          properties:
 *              username:
 *                  type: string
 *                  default: Jane Doe
 *              password:
 *                  type: string
 *                  default: stringPassword123
 *      
 */


import mongoose from 'mongoose';
const { Schema } = mongoose;

const userSchema = new Schema({
    username:{
        type:String,
        required:true,
        unique: true,
    },
    email:{
        type:String,
        required:true,
        unique: true,
    },
    password:{
        type:String,
        required:true,
    },
    pfp:{
        type:String,
        required:false,
    },
    desc:{
        type:String,
        default:"",
        required:false,
    },
},{
        timestamps:true
});

export default mongoose.model("User", userSchema);