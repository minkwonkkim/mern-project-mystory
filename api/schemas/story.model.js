/**
 * @openapi
 * components:
 *   schemas:
 *     Story:
 *       type: object
 *       required:
 *         - _id
 *         - userId
 *         - title
 *         - story
 *         - image
 *         - category
 *       properties:
 *         _id:
 *           type: Object(string)
 *           description: The auto-generated unique id of the book given by mongoose & mongodb
 *         userId:
 *           type: string
 *           description: The unique id of the user that wrote the comment
 *         title:
 *           type: string
 *           description: The title of the story
 *         image:
 *           type: string
 *           description: The link to the image. The images are stored in a cloud.
 *         category:
 *           type: string
 *           enum: ['Horror','Funny', 'Sad', 'Music', 'Love', 'Wholesome', 'Education', 'Tips', 'Art', 'Fictional']
 *           description: Category of the story
 *         totalStars:
 *           type: number
 *           default: 0
 *           description: The total number of stars received
 *         starNumber:
 *           type: number
 *           default: 0
 *           description: How many time the story received a star
 *         createdAt:
 *           type: string
 *           format: date
 *           description: The auto-generated date of creation given by mongoose & mongodb
 *
 *     CreateStoryInput:
 *         type: object
 *         required:
 *             - title
 *             - story
 *             - image
 *             - category
 *         properties:
 *             title:
 *                 type: string
 *                 default: stringTitle123
 *             category:
 *                 type: string
 *                 enum: ['Horror','Funny', 'Sad', 'Music', 'Love', 'Wholesome', 'Education', 'Tips', 'Art', 'Fictional']
 *                 default: Horror
 *             image:
 *                 type: string
 *                 default: "https://linktoimg.com"
 *             story:
 *                 type: string
 *                 default: Once upon a time
 */



import mongoose from 'mongoose';
const { Schema } = mongoose;

const storySchema = new Schema({
        userId: {
                type: String,
                required: true,
        },
        title:{
                type: String,
                required: true,
        },
        story: {
                type: String,
                required: true,
        },
        image: {
                type:String,
                required: true,
        },
        totalStars: {
                type: Number,
                default:0,
        },
        starNumber: {
                type: Number,
                default:0,
        },
        category:{
                type: String,
                required: true,
                enum:['Horror','Funny', 'Sad', 'Music', 'Love', 'Wholesome', 'Education', 'Tips', 'Art', 'Fictional'],
        }

},{
        timestamps:true
});

export default mongoose.model("Story", storySchema);