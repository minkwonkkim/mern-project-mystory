import Story from "../schemas/story.model.js"
import createError from "../utils/createError.js";

export const createStory = async (req, res, next) =>{
    //req comes from the jwt
    const newStory = new Story({
        userId: req.userId,
        ...req.body,
    });

    try {
        const savedStory = await newStory.save();
        res.status(201).json(savedStory);
    } catch (error) {
        next(error);
    }

};

export const deleteStory = async (req, res, next) =>{
    try {
        const story = await Story.findById(req.params.id);
        if(!story)
            return next(createError(404, "Story not found"));
        if(story.userId !== req.userId) 
            return next(createError(403, "You're not allowed to delete this story"));
        await Story.findByIdAndDelete(req.params.id);
        res.status(200).send("Story deleted");
    } catch (error) {
        next(error);
    }
}

export const getStory = async (req, res, next) =>{
    let checkNull;
    try {
        const story = await Story.findById(req.params.id);
        checkNull = story;
        res.status(200).send(story);
    } catch (error) {
        if(!checkNull)
            next(createError(404, "Story not found"));
        next(error);
    }
}
export const getStories = async (req, res, next) =>{

    //req.query contains all elements after ? in request pathname
    //e.g localhost:8080/api/stories?category=Love&search=blabla
    //req.query will contain req.query.category = Love, req.query.search=blabla etc
    const qry = req.query;

    const filters = {
        //basically if there's a category then && create category object {} and spread it ...
        ...(qry.category && {category:qry.category}),
        ...(qry.userId && {userId:qry.userId}),
...(qry.search && {title: {$regex: qry.search, $options: "i"}}),
        //option i = ignore lowercase uppercase
        ...(qry.search && {title: {$regex: qry.search, $options: "i"}}),
        ...(qry.search && {story: {$regex: qry.search, $options: "i"}}),
        
    }

    try {
        let stories;

        if(qry.slide)
            stories = await Story.find().limit(5);
        else
            stories = await Story.find(filters).sort({[qry.sort]:-1});
        
        res.status(200).send(stories);
    } catch (error) {
        next(error);
    }
}

export const putStory = async (req,res,next)=>{
    try {
        const story = await Story.findById(req.params.id);

        if(!story)
            return next(createError(404, "can't find this story"));
        

        if(story.userId !== req.userId) 
            return next(createError(403, "Can't update, it's not your story"));

        await Story.findByIdAndUpdate(story._id, {title: req.body.title});
        res.status(200).send ("updated story");
        
    } catch (error) {
        next(error);
    }
}