import User from "../schemas/user.model.js"
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import createError from "../utils/createError.js";

export const register = async (req, res, next)=>{
    try{
        //encrypt password as soon as received
        const hash = bcrypt.hashSync(req.body.password, 5);

        //creating newUser instance thanks to the User Schema
        //make sure to replace password to its encrypt one
        const newUser = new User({
            ...req.body,
            password: hash
        });

        //wait for creating user operation to finish, then save it to the db
        await newUser.save();

        //201 is success and created
        res.status(201).send();
    }catch(error){
        next(error);
    }
};

export const login = async (req, res, next)=>{
    try {
        //finding only one (findOne) user in the db with the username = username in body of post request
        const user = await User.findOne({username:req.body.username});
        //sending error if user null i.e not found
        if(!user) return next(createError(400, "User not found"));

        //comparing requested password with the one in db
        const isCorrect = bcrypt.compareSync(req.body.password, user.password);
        //sending error if wrong password, putting also wrong username for security purposes and hide code behaviour
        if (!isCorrect) return next(createError(400, "Wrong password or username"));

        //creating token if user was identified and logged in successfully
        //token is created with jwt (JsonWebToken) library. we sign and pass through infos from user with header and payload, and secret JWT_SECRET saved in .env
        //header+payload + JWT_SECRET = Original Signature         |       Original Signature + header+payload = JWT
        //server receives JWT after request. JWT = Original signature + newHeader+newPayload
        //server creates Test Signature = newHeader+newPayload + JWT_SECRET
        //if Test Signature =/= Original Signature, knowing that JWT_SECRET didn't change, then user changed newHeader and newPayload
        //if someone logs in and then tries to change the route to someone else's id (data in header or payload) then new signature will be different from old one
        //if logged in as user1 then cookie will include id1. If then tried to send delete request on users/id2, then token different and we can block.
        const token = jwt.sign({
            id:user._id,
        }, process.env.JWT_SECRET)

        //_doc is where is the information we need is located. user alone contains too much information that we do not need for login
        const {password, ...info} = user._doc;

        //put the accessToken jwt signed in a cookie and send it to the page
        //write httpOnly so that only http request can modify it
        res.cookie("accessToken", token, {
            httpOnly: true,
        }).status(200).send(info);

    } catch (error) {
        next(error);
    }
    
}

export const logout = async (req, res)=>{
    res.clearCookie("accessToken", {
        sameSite:"none",
        secure: true,
    })
    .status(200)
    .send("User logged out")
};