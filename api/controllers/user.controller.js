import User from "../schemas/user.model.js";
import createError from "../utils/createError.js";

export const deleteUser = async (req, res, next) =>{

    const user = await User.findById(req.params.id);

        if(!user){
            return next(createError(404, "can't delete this account"));
        }

        if(req.userId !== user._id.toString())
            return next(createError(403, "You can't delete this account ; it is not yours"));
            
        //request param contains what's after ':' in the route path
        //exemple api/auths/login/:id means param contains id attribute. So we can call params.id
        await User.findByIdAndDelete(req.params.id);
        res.status(200).send("account deleted");

}

export const getUser = async (req, res) =>{
    const user = await User.findById(req.params.id);

    res.status(200).send(user);

}