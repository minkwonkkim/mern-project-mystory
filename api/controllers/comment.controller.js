import Comment from "../schemas/comment.model.js";
import createError from "../utils/createError.js";
import Story from "../schemas/story.model.js"

export const createComment = async (req,res,next)=>{
    const newComment = new Comment({
        userId: req.userId,
        storyId: req.body.storyId,
        star: req.body.star,
        txt : req.body.txt,
    });

    try {
        const comment = await Comment.findOne({
            storyId: req.body.storyId,
            userId: req.userId
        });

        const savedComment = await newComment.save();

        await Story.findByIdAndUpdate(req.body.storyId, {$inc: {totalStars : req.body.star, starNumber:1}});

        res.status(201).send(savedComment);
    } catch (error) {
        next(error);
    }
}

export const getComments = async (req,res,next)=>{
    try {
        const comments = await Comment.find({storyId: req.params.storyId});

        res.status(200).send(comments)
    } catch (error) {
        next(error);
    }
}

export const deleteComment = async (req,res,next)=>{
    try {
        const comment = await Comment.findById(req.params.commentId);

        if(!comment)
            return next(createError(404, "can't find this comment"));
        

        if(comment.userId !== req.userId) 
            return next(createError(403, "Can't delete, it's not your comment"));

        await Comment.findByIdAndDelete(comment._id);
        res.status(200).send ("deleted comment");
        
    } catch (error) {
        next(error);
    }
}

export const putComment = async (req,res,next)=>{
    try {
        const comment = await Comment.findById(req.params.commentId);

        if(!comment)
            return next(createError(404, "can't find this comment"));
        

        if(comment.userId !== req.userId) 
            return next(createError(403, "Can't update, it's not your comment"));

        await Comment.findByIdAndUpdate(comment._id, {txt:req.body.txt});
        res.status(200).send ("updated comment");
        
    } catch (error) {
        next(error);
    }
}