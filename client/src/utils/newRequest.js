import axios from "axios";

//withCredentials because using cookies
const newRequest = axios.create({
    baseURL: import.meta.env.VITE_API_BASE_URL,
    withCredentials: true,
});

export default newRequest;