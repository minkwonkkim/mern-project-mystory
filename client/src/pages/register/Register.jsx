import React, { useState } from "react";
import newRequest from "../../utils/newRequest";
import "./Register.scss";
import upload from "../../utils/upload";
import {useNavigate} from "react-router-dom"

function Register() {
  const [file, setFile] = useState(null);
  const [user, setUser] = useState({
    username: "",
    email: "",
    password: "",
    pfp: "",
    desc: "",
  });
  
  const[failed,setFailed] = useState(false);

  const navigate = useNavigate();

  const handleChange = (e) => {
    setFailed(false);

    setUser(prev =>{
      return{...prev, [e.target.name]: e.target.value}
    })
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    
    const url = await upload(file);
    try {
      await newRequest.post("/auths/register", {
        ...user,
        pfp:url
      });
      navigate("/");
    } catch (error) {
      if(error.response.status === 500)
        setFailed(true);
      console.log(error);
    }
  }

  return (
    <div className="register">
      <form onSubmit={handleSubmit}>
        <div className="left">
          <h1>Create a new account</h1>
          <label htmlFor="">Username</label>
          <input
            name="username"
            type="text"
            required
            placeholder="johndoe"
            onChange={handleChange}
          />
          <label htmlFor="">Email</label>
          <input
            name="email"
            type="email"
            placeholder="email"
            required
            onChange={handleChange}
          />
          <label htmlFor="">Password</label>
          <input name="password" type="password" required onChange={handleChange}/>
          <label htmlFor="">Profile Picture</label>
          <input type="file" required onChange={e=>setFile(e.target.files[0])}/>
          <button type="submit">Register</button>
        </div>
        <div className="right">
          <label htmlFor="">Description</label>
          <textarea
            placeholder="Describe yourself"
            name="desc"
            id=""
            cols="30"
            rows="10"
            onChange={handleChange}
          ></textarea>
        </div>
      </form>
      {failed && <div className="warn">Can't create account, username or email already used</div>}
    </div>
  );
}

export default Register;