import React, { useState } from "react";
import { Link } from 'react-router-dom'
import "./MyStories.scss"
import newRequest from "../../utils/newRequest";
import {useQuery , useMutation} from '@tanstack/react-query';


const MyStories = () => {

  const currentUser = JSON.parse(localStorage.getItem('currentUser'));

  const { isLoading, error, data} = useQuery({
    queryKey: ["stories"],
    queryFn: () =>
      newRequest
        .get(
          `/stories?userId=${currentUser._id}`
        )
        .then((res) => {
          return res.data;
        }),
  });

  const mutationDel = useMutation({
    mutationFn: (story) =>{
        return newRequest.delete(`/stories/${story._id}`, story)
    },
    onSuccess:()=>{
        queryClient.invalidateQueries(["stories"]);
    }
})

  return (
    <div className='myStories'>
      <div className="container">
        <div className="title">
          <h1>My Stories</h1>
          <Link to="/addStories">
            <button>
              Add new Story
            </button>
          </Link>
        </div>
        <table>
          <thead>
            <tr>
              <th>Image</th>
              <th>Title</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
          {isLoading ? <img src="/imgAssets/loading.gif" alt="" /> : error ? null : data.map
          (
            story_ => 
              (
              <tr key={story_._id}>
                <td>
                  <Link to={`/story/${story_._id}`}><img className='image'  src={story_.image} alt="" /></Link>
                </td>
                <td>
                  {story_.title}
                </td>
                <td>
                  <img className='delete' onClick={()=>{mutationDel.mutate(story_);}} src="imgAssets/delete.png" alt="" />
                </td>
              </tr>
            )
          )}
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default MyStories