import React from "react";
import "./Story.scss";
import { useParams } from "react-router-dom";
import {useQuery} from "@tanstack/react-query";
import newRequest from "../../utils/newRequest";
import Comments from "../../components/comments/Comments";

function Story() {

  const { id } = useParams();

  const { isLoading, error, data } = useQuery({
    queryKey: ["story"],
    queryFn: () =>
      newRequest.get(`/stories/single/${id}`).then((res) => {
        return res.data;
      }),
  });

  const userId = data?.userId;

  const {
    isLoading: isLoadingUser,
    error: errorUser,
    data: dataUser,
  } = useQuery({
    queryKey: ["user"],
    queryFn: () =>
      newRequest.get(`/users/${userId}`).then((res) => {
        return res.data;
      }),
    enabled: !!userId,
  });

  return (
    <div className="story">
  {isLoading ? <img src="/imgAssets/loading.gif" alt="" /> : error ? "Something went wrong" : 
      <div className="container">
        <h1>{data.title}</h1>
        {isLoadingUser ? <img src="/imgAssets/loading.gif" alt="" /> : errorUser ? "Something went wrong with User" : 
        <div className="user">
          <img
            className="pp"
            src={dataUser.pfp || "../src/default_pfp.png"}
            alt=""
          />
          <span>{dataUser.username}</span>
          {!isNaN(data.totalStars/data.starNumber) && (
              <div className="stars">
                {Array(Math.round(data.totalStars/data.starNumber)).fill().map((item,i)=>(
                  <img src="/imgAssets/star.png" alt="" key={i}/>
                ))}
                <span>{!isNaN(Math.round(data.totalStars/data.starNumber)) && Math.round(data.totalStars/data.starNumber) || "0"}</span>
              </div>)
              }
        </div>
        }
        <div className="storyImg">
          <img
            src={data.image}
            alt=""
          />
        </div>
        <h2>The Story :</h2>
        <p>
          {data.story}
        </p>
        
        <div className="author">
          <h2>About the author</h2>
          {isLoadingUser ? <img src="/imgAssets/loading.gif" alt="" /> : errorUser ? "Something went wrong with User" : (
          <div className="user">
            <img
              src={dataUser.pfp || "../src/default_pfp.png"}
              alt=""
            />
            <div className="info">
              <span>{dataUser.username}</span>
            </div>
          </div>
          )}
          {isLoadingUser ? <img src="/imgAssets/loading.gif" alt="" /> : errorUser ? "Something went wrong with User" : 
          <div className="box">
            <p>
            {dataUser.desc || "No description given by author"}
            </p>
          </div>
          }
        </div>
        <Comments storyId={id}/>
      </div>}
    </div>
  );
}

export default Story;