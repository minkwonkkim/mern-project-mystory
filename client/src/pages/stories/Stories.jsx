import React, { useEffect, useState, Link } from "react";useState
import "./Stories.scss";
import StoryCard from "../../components/card/StoryCard";
import newRequest from "../../utils/newRequest";
import {useQuery} from '@tanstack/react-query';
import { useLocation } from "react-router-dom";

function Stories() {
  const [sort, setSort] = useState("totalStars");
  const [open, setOpen] = useState(false);

  const {search} = useLocation();

  const { isLoading, error, data, refetch } = useQuery({
    queryKey: [search],
    queryFn: () =>
      newRequest
        .get(
          search !== "" ? `/stories${search}&sort=${sort}` : `/stories?sort=${sort}`
        )
        .then((res) => {
          return res.data;
        }),
  });

  const reSort = (type) => {
    setSort(type);
    setOpen(false);
  };

  useEffect(() => {
    refetch();
  }, [sort]);
  refetch();
  return (
    <div className="stories">
      <div className="container">
        <h1>Stories</h1>
        <p>
          All the stories here !
        </p>
        <div className="menu">
          <div className="sort">
            <span className="sortBy" >Sort by</span>
            <span className="sortType" onClick={() => setOpen(!open)}>
              {sort === "totalStars" ? "Popular" : "Newest"}
            </span>
            {open && (
              <div className="rightMenu">
                {sort === "totalStars" ? (
                  <span onClick={() => reSort("createdAt")}>Newest</span>
                ) : (
                  <span onClick={() => reSort("totalStars")}>Popular</span>
                  )}
              </div>
            )}
          </div>
        </div>
        <div className="cards">
          {isLoading ? <img src="/imgAssets/loading.gif" alt="" /> : error ? "Something went wrong" : data.map(story_ => (
            <StoryCard key={story_._id} story={story_} />
          ))}
        </div>
      </div>
    </div>
  );
}

export default Stories;