import "./AddStories.scss";
import React, { useState } from "react";
import newRequest from "../../utils/newRequest";
import upload from "../../utils/upload";
import {useNavigate} from "react-router-dom"

const AddStories = () => {
  const [file, setFile] = useState(null);
  const [story, setStory] = useState({
    title: "",
    story: "",
    image: "",
    category: "",
  });

  const currentUser = JSON.parse(localStorage.getItem('currentUser'));

  const navigate = useNavigate();
  console.log(story);
  const handleChange = (e) => {
    setStory(prev =>{
      return{...prev, [e.target.name]: e.target.value}
    })
  }

  const handleSubmit = async (e) => {
    e.preventDefault();
    
    const url = await upload(file);
    try {
      await newRequest.post("/stories", {
        ...story,
        image:url,
        userId:currentUser._id,
      });
      navigate("/");
    } catch (error) {
      console.log(error);
    }
  }

  if(!currentUser)
    return (<div className="addStories">Not authentified, can't access this page :D</div>);

  return (
    <div className="addStories">
      <div className="container">
        <h1>Add a new Story</h1>
        <form onSubmit={handleSubmit}>
          <div className="info">
            <label htmlFor="">Title</label>
            <input
              name="title"
              type="text"
              required
              placeholder="A catching title for your Story !"
              onChange={handleChange}
            />
            <label htmlFor="">Category</label>
            <select name="category" default="Horror" id="cats" required onChange={handleChange}>
              <option value="Horror">Horror</option>
              <option value="Funny">Funny</option>
              <option value="Sad">Sad</option>
              <option value="Love">Love</option>
              <option value="Wholesome">Wholesome</option>
              <option value="Education">Education</option>
              <option value="Tips">Tips & Tricks</option>
              <option value="Art">Art</option>
              <option value="Fictional">Fictional</option>
            </select>
            <label htmlFor="">Upload your story's picture !</label>
            <input required type="file" onChange={e=>setFile(e.target.files[0])}/>
            <label htmlFor="">Your Story</label>
            <textarea name="story" id="" placeholder="Tell your story here !" cols="0" rows="16" onChange={handleChange} required></textarea>
            <button>Create</button>
          </div>
        </form>
      </div>
    </div>
  );
};

export default AddStories;