import React from "react";
import NavBar from "./components/navbar/Navbar";
import Home from "./pages/home/Home";
import Stories from "./pages/stories/Stories";
import MyStories from "./pages/myStories/MyStories";
import Story from "./pages/story/Story";
import AddStories from "./pages/addStories/AddStories";
import Login from "./pages/login/Login";
import Register from "./pages/register/Register";
import "./App.scss";
import {
  QueryClient,
  QueryClientProvider,
  useQuery,
} from '@tanstack/react-query';

const queryClient = new QueryClient();

import {
  createBrowserRouter,
  RouterProvider,
  Outlet
} from "react-router-dom";

function App() {

  const Layout = ()=>{
    return (
      <>
      <QueryClientProvider client={queryClient}>
        <div className="bg"/>
          <NavBar/>
          <Outlet/>
      </QueryClientProvider>
      </>
    )
  }

  //from react-router-dom
  const router = createBrowserRouter([
    {
      path: "/",
      element: <Layout/>,
      children:[
        {
          path:"/",
          element:<Home/>
        },
        {
          path:"/stories",
          element:<Stories/>
        },
        {
          path:"/story/:id",
          element:<Story/>
        },
        {
          path:"/myStories",
          element:<MyStories/>
        },
        {
          path:"/addStories",
          element:<AddStories/>
        },
        {
          path:"/login",
          element:<Login/>
        },
        {
          path:"/register",
          element:<Register/>
        }
      ]
    },
  ]);

  return (
    <div>
      <RouterProvider router={router} />
    </div>
  )
}

export default App
