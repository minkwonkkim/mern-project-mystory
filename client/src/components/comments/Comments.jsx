import React from 'react'
import "./Comments.scss"
import Comment from "../comment/Comment"
import {useQuery, useMutation, useQueryClient} from "@tanstack/react-query"
import newRequest from '../../utils/newRequest'

const Comments = ({storyId}) => {

    const queryClient = useQueryClient();

    const { isLoading, error, data } = useQuery({
        queryKey: ["comments"],
        queryFn: () =>
          newRequest.get(`/comments/${storyId}`).then((res) => {
            return res.data;
          }),
    });

    const mutation = useMutation({
        mutationFn: (comment) =>{
            return newRequest.post('/comments', comment)
        },
        onSuccess:()=>{
            queryClient.invalidateQueries(["comments"]);
        }
    })

    const handleSubmit = e=>{
        e.preventDefault();
        const txt = e.target[0].value;
        const star = e.target[1].value;
        mutation.mutate({storyId, txt, star});
    }

    return (
        <div className="comments">
            <h2>Comments</h2>
            {isLoading ? <img src="/imgAssets/loading.gif" alt="" /> : error ? "Something wrong with the comments" :
                !data ? <p>"No comments here" </p>:
                data.map(comment => (
                    <Comment key={comment._id} comment={comment}/>
                    ))}
            <div className="add">
                <h3>Commment here</h3>
                <form action="" className="addForm" onSubmit={handleSubmit}>
                    <textarea placeholder="Write your comment here"/>
                    <select name= "" id="">
                        <option value={1}>1</option>
                        <option value={2}>2</option>
                        <option value={3}>3</option>
                        <option value={4}>4</option>
                        <option value={5}>5</option>
                    </select>
                    <button>Send</button>
                </form>
            </div>
        </div>
    )
}


export default Comments