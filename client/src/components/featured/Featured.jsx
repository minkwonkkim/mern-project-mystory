import React, { useState } from 'react'
import "./Featured.scss"
import {useNavigate} from "react-router-dom"

const Featured = () => {
    const [input, setInput] = useState("");
    const navigate = useNavigate();

    const handleSubmit = ()=>{
        if(input !== "")
            navigate(`stories?search=${input}`);
    }
    
    const handleEnter = (e) =>{
        if(e.key === 'Enter')
            handleSubmit();
    }

    return (
        <div className='featured'>
            <div className="container">
                <div className="left">
                    <h1>Search for stories to read !</h1>
                    <div className="search">
                        <div className="searchInput">
                            <img src="./imgAssets/search.png" alt="" />
                            <input type="text" placeholder="Type here !" onKeyDown={handleEnter} onChange={e=>setInput(e.target.value)}/>
                        </div>
                        <button onClick={handleSubmit}>Search</button>
                    </div>
                </div>
                <div className="mid">
                    <img src="./imgAssets/arrowdown.png" alt="" />
                </div>
                <div className="right">
                    <img src="./imgAssets/imgFeat.png" alt=""/>
                </div>
            </div>
        </div>
    )
}

export default Featured