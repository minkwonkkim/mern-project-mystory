import React, { useState } from 'react'
import { Link, useLocation } from "react-router-dom"
import newRequest from '../../utils/newRequest';
import "./Navbar.scss"
import { useNavigate } from "react-router-dom"

const Navbar = () => {

    const [openMenu, setOpenMenu] = useState(false);
    const {pathname} = useLocation();
    
    const currentUser = JSON.parse(localStorage.getItem('currentUser'));

    const navigate = useNavigate();

    const handleLogout = async () =>{
        try {
            await newRequest.post("/auths/logout");
            localStorage.setItem("currentUser", null);
            navigate("/");
        } catch (error) {
            console.log(err);
        }
    }

    return (

        <div className={pathname !== "/" ? 'navbar active' : 'navbar'}>
            <div className="container">
                <div className="logo">
                    <Link to="/" className="link">
                        <span className='text'>myStory</span>
                    </Link>
                </div>

                <div className="links">
                    <Link className="link" to="/Stories">Stories</Link>
                    {!currentUser && 
                        <Link className="link" to="/login">
                            <span>Sign in</span>
                        </Link>
                    }
                    {!currentUser && 
                        <Link className="link" to="/register">
                            <button className={pathname == "/" ? 'active' : ''}>Sign up</button>
                        </Link>
                        }
                    {currentUser && (
                        <div className="user" onClick={(e)=>{e.preventDefault(); setOpenMenu(!openMenu);}}>
                            <img src ={currentUser.pfp || "/src/default_pfp.png"} alt=""/>
                            <span>{currentUser.username}</span>
                            {openMenu && 
                            <div className="menu">
                                <Link className="link" to ="/myStories">myStories</Link>
                                <Link className="link" to ="/addStories">Add new Story</Link>
                                <Link className="link" onClick={handleLogout}>Logout</Link>
                            </div>}
                        </div>
                    )}
                </div>
            </div>

                    <hr className={pathname !== "/" ? 'active' : ''}/>
                    <div className={pathname !== "/" ? 'active categories' : 'categories'}>
                        <Link className="link menuLink" to="/Stories?category=Horror">Horror</Link>
                        <Link className="link menuLink" to="/Stories?category=Funny">Funny</Link>
                        <Link className="link menuLink" to="/Stories?category=Sad">Sad</Link>
                        <Link className="link menuLink" to="/Stories?category=Music">Music</Link>
                        <Link className="link menuLink" to="/Stories?category=Love">Love</Link>
                        <Link className="link menuLink" to="/Stories?category=Wholesome">Wholesome</Link>
                        <Link className="link menuLink" to="/Stories?category=Education">Education</Link>
                        <Link className="link menuLink" to="/Stories?category=Tips">Tips and tricks</Link>
                        <Link className="link menuLink" to="/Stories?category=Art">Art</Link>
                        <Link className="link menuLink" to="/Stories?category=Fictional">Fictional</Link>
                    </div>
        </div>
    )
}

export default Navbar