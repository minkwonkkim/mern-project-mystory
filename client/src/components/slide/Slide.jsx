import React from 'react'
import "./Slide.scss"
import { useState } from 'react';
import ReactSimplyCarousel from 'react-simply-carousel'
import {storiesFeatured} from "../../mockdata"
import {useQuery} from '@tanstack/react-query';
import StoryCard from '../card/StoryCard';
import newRequest from '../../utils/newRequest';

const Slide = () => {
  const [activeSlideIndex, setActiveSlideIndex] = useState(0);

  const { isLoading, error, data } = useQuery({
    queryKey: ['stories'],
    queryFn: () =>
      newRequest.get(`/stories`).then(res=>{
        return res.data;
      }),
    })

  return (
    <div className="slide">
        <div className="container">
            <ReactSimplyCarousel
            activeSlideIndex={activeSlideIndex}
            onRequestChange={setActiveSlideIndex}
            forwardBtnProps={{
            //here you can also pass className, or any other button element attributes
            style: {
                alignSelf: 'center',
                background: 'black',
                border: 'none',
                borderRadius: '50%',
                color: 'white',
                cursor: 'pointer',
                fontSize: '20px',
                height: 30,
                lineHeight: 1,
                textAlign: 'center',
                width: 30,
            },
            children: <span>{`>`}</span>,
            }}
            backwardBtnProps={{
            //here you can also pass className, or any other button element attributes
            style: {
                alignSelf: 'center',
                background: 'black',
                border: 'none',
                borderRadius: '50%',
                color: 'white',
                cursor: 'pointer',
                fontSize: '20px',
                height: 30,
                lineHeight: 1,
                textAlign: 'center',
                width: 30,
            },
            children: <span>{`<`}</span>,
            }}
            responsiveProps={[
            {
                itemsToShow: 6,
                itemsToScroll: 1,
                minWidth: 768,
                swipeRatio: 1
            },
            ]}
            speed={400}
            easing="linear"
            >
                {/* here you can also pass any other element attributes. Also, you can use your custom components as slides */}
                {isLoading ? <img src="/imgAssets/loading.gif" alt="" /> : error ? storiesFeatured.map(story_=>(
                    <StoryCard key={story_.id} story={story_}/>
                )) : data.map(story_=>(
                    <StoryCard key={story_._id} story={story_}/>
                ))}
            </ReactSimplyCarousel>
        
        </div>
    </div>
  )
}

export default Slide