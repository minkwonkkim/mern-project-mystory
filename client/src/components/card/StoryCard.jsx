import React from 'react'
import { Link } from "react-router-dom"
import "./StoryCard.scss"
import {useQuery} from '@tanstack/react-query';
import newRequest from '../../utils/newRequest';

const StoryCard = ({story}) => {
  
  const id = story._id;
  const userId = story.userId;
  const pathTo = "/story/"+id;
  const rate = Math.round(story.totalStars/story.starNumber);

  const { isLoading, error, data } = useQuery({
    queryKey: [userId],
    queryFn: () =>
      newRequest
        .get(
          `/users/${userId}`
        )
        .then((res) => {
          return res.data;
        }),
  });

  return (
    <Link to ={pathTo} className="link">
      <div className="storyCard">
          <div className="container">
            <img src={story.image} alt="" />
            <div className="info">
              <div className="baseInfo">
                {isLoading ? (
                  <img src="/imgAssets/loading.gif" alt="" />
                ) : error ? (
                  "Something went wrong!"
                ) : (
                    <img src={data.pfp || "../src/default_pfp.png"} alt="" />
                )}
                <div className="texts">
                  <h2>{story.title}</h2>
                  <span>{story.username}</span>
                </div>
              </div>
              <div className="rating">
                <img src="imgAssets/star.png" alt="" />
                <div className="span">{!isNaN(rate) && rate || "0"}</div>
              </div>
            </div>
          </div>
      </div>
    </Link>
  )
}

export default StoryCard