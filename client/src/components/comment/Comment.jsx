import { useQuery, useMutation, useQueryClient } from "@tanstack/react-query";
import React, { useState } from "react";
import newRequest from "../../utils/newRequest";
import "./Comment.scss";

const Comment = ({ comment }) => {

  const queryClient = useQueryClient();

  const currentUser = JSON.parse(localStorage.getItem('currentUser'));

  const checkId = currentUser._id.toString() == comment.userId;

  const { isLoading, error, data } = useQuery(
    {
      queryKey: [comment.userId],
      queryFn: () =>
        newRequest.get(`/users/${comment.userId}`).then((res) => {
          return res.data;
        }),
    },
  );

  const mutationDel = useMutation({
        mutationFn: (commentId) =>{
            return newRequest.delete(`/comments/${commentId}`, comment)
        },
        onSuccess:()=>{
            queryClient.invalidateQueries(["comments"]);
        }
    })

    const mutationUpd = useMutation({
        mutationFn: (txt_) =>{
            return newRequest.put(`/comments/${comment._id}`, {txt : txt_})
        },
        onSuccess:()=>{
            queryClient.invalidateQueries(["comments"]);
        }
    })

  const handleClickDel = e => {
    e.preventDefault();
    mutationDel.mutate(comment._id);
  }

  const [upd, setUpd] = useState(false);

  const handleClickUpd = e => {
    e.preventDefault();

    setUpd(!upd);
    if(upd){
        mutationUpd.mutate(comment.txt);
    }
  }

  return (
    <div className="review">
      {isLoading ? (
        <img src="/imgAssets/loading.gif" alt="" />
      ) : error ? (
        "error"
      ) : (
        <div className="contain">
            <div className="user">
                <img className="pfp" src={data.pfp || "/src/default_pfp.png"} alt="" />
                <div className="info">
                    <span>{data.username}</span>
                </div>
            </div>
            {checkId ? 
                <div className="change">
                    <img onClick={handleClickDel} src="/imgAssets/delete.png" alt="" />
                    <img onClick={handleClickUpd} src="/imgAssets/update.png" alt="" />
                </div>
                : ""}
        </div>
      )}
        <div className="stars">
            {Array(comment.star)
            .fill()
            .map((item, i) => (
                <img src="/imgAssets/star.png" alt="" key={i} />
            ))}
            <span>{comment.star}</span>
        </div>
      {upd ? <textarea defaultValue={comment.txt} onChange={(e) => {comment.txt = e.target.value;}}></textarea> : <p>{comment.txt}</p>}
    </div>
  );
};

export default Comment;