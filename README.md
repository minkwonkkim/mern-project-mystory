# MERN Project myStory

    Deployed : https://mystoryfront.onrender.com/
    (Images can take a while to load because free host)

## Getting started IMPORTANT

    Use node.js version 18.13.0
    User yarn version 3.4.1

    Using Cloudinary to host images.
    MongoDB cloud Atlas for db.

## Installation and running on server + client

Tested on VSCode
Again, use node.js version 18.13.0 and yarn version 3.4.1

If you already have node.js, install on windows :

```
npm install -g node@18.13.0

```

Install yarn :
```
npm install --global yarn

```

clone the git


Open two terminals

First terminal (Server) :

```
cd api
yarn
yarn start
```

```
Make sure you can execute scripts.
If yarn start doesn't work on windows, open a powershell in ADMIN mode
then type : Set-ExecutionPolicy RemoteSigned
```

Second terminal (Client) :

```
cd client
yarn
yarn run dev
```

MAKE SURE that the link provided in the client terminal is the same as the one in the file api/.env
in the api folder

go to the link provided in the client terminal (should be http://localhost:5173)

## View API DOC With Swagger

    After initialisation go to the link : 
    http://localhost:8080/api-docs

    Or whatever is written on the server console to get to the API doc.

    To export request to postman, go to http://localhost:8080/api-docs.json
    copy the json and import it to postman :
    Connect to postman
    Collections > import (top left) > raw text > paste json > don't forget to click on
    Show import settings > check "Copy collections to workspace"

POSTMAN collection requests used to first test :
[click here](https://www.postman.com/docking-module-observer-46628538/workspace/my-workspace/collection/13551566-2d996ba5-9881-4ef1-8a60-f9573a6d00fe?action=share&creator=13551566)

POSTMAN collection requests from api-docs for second batch of tests
[click here](https://www.postman.com/docking-module-observer-46628538/workspace/mystory-from-swagger/collection/13551566-6829e765-e8fa-4278-8d13-5d2877a2021a?action=share&creator=13551566)

## What is it ?

    Concept of website is to allow users to post fun short stories and include a picture with it.
    Users can add stories, rate other people stories and comment.
    You can navigate through the website by signing up or in, logging out, using the searchbar, 
    using the navigation bar, accessing the list of stories by user
    Can also click on stories on the top right to access the full list of Stories without filters
    
    On the home page, if you scroll down, you find a slider with a sample of stories.

    After signing in, clicking on your username, then using the menu to add stories, 
    or see your current stories

## Functionnalities

    Sign up
    Login / Logout
    Logins used with JWTokens and Cookies (security measures)

    If no profile picture when signing in, default one from files will be used.

    Searchbar (search title or story)

    Infinite scroll carousel list (in the home page, by scrolling down)

    Stories list :
        - Access by clicking on "Stories" on top right
        - Can sort via numbers of stars given
        - Can sort via Newest
        - Can sort via categories by clicking on top navigation bar
        - Can sort via title and story text regex, on the home page search bar

    Single Story page :
        - Can comment (CRUD)
        - Can add rating out of 5 (Create)
        - Average rating of that story will be displayed (Update)

    Comment (Full CRUD): 
    Can Create, Read, Update, Delete on story page
    Can't Update or delete comments that's not yours

    After signing up :
        - click top right username
        - myStories list all stories from user
            - can delete and access single story by clicking on image
        - add new story
            - form page where can add a new story
        - Logout
            - clears cookie and logs out

## Deployement

    Deployed with render
    https://mystoryfront.onrender.com/
    (Images can take a while to load because free host)
***
